import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

///
/// Charging APP Enter
/// Config Localizations, Theme, Route
///
class ChargingApp extends StatelessWidget {
  const ChargingApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      home: Builder(
          builder: (ctx) => Center(
                child: Text(AppLocalizations.of(ctx)!.title),
              )),
    );
  }
}
